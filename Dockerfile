FROM messense/rust-musl-cross:x86_64-musl as build

WORKDIR /home/rust/src
RUN USER=root cargo new --bin image_upload
WORKDIR /home/rust/src/image_upload

COPY Cargo.lock Cargo.toml ./
RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src
RUN touch src/main.rs
RUN cargo install --path .

FROM scratch
COPY --from=build /root/.cargo/bin/image_upload .
CMD ["./image_upload"]
