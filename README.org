#+TITLE: image_upload

* Setup
Configuration is done 100% via environment variables. =image_upload= does *not*
assume where they originate from (e.g. no =.env= file will get loaded
automatically).

For example, if you want to use =direnv=:

#+BEGIN_SRC sh
cp .env.example .env
# edit .env to your liking
direnv allow
cargo run
#+END_SRC

Or just manually:

#+BEGIN_SRC sh
SERVER_PORT=8000 STORAGE_PATH=storage cargo run
#+END_SRC

** Docker
There is a provided Docker image and =docker-compose= config:

#+BEGIN_SRC sh
docker-compose up -d
#+END_SRC

Then access API on =localhost:$PORT=.
* API Reference
** =POST /upload=
Body (JSON):

| key     | type          | description                 |
|---------+---------------+-----------------------------|
| =links= | ~Vec<String>~ | An array of links to images |

#+BEGIN_SRC restclient :session
POST http://localhost:8000/upload

{
    "links": [
        "https://rustacean.net/assets/rustacean-orig-noshadow.png",
        "https://docs.rs/rust-logo-20190720-1.38.0-nightly-95b1fe560.png"
    ]
}
#+END_SRC

#+RESULTS:
: #+BEGIN_SRC js
: // POST http://localhost:8000/upload
: // HTTP/1.1 200 OK
: // content-length: 0
: // date: Fri, 13 Sep 2019 09:12:12 GMT
: // Request duration: 3.602056s
: #+END_SRC

** =GET /view/preview/:filename=
Query parameters:

| key      | type   | description                                             |
|----------+--------+---------------------------------------------------------|
| filename | ~String~ | The filename used in the =links= section                |

#+BEGIN_SRC restclient :session
GET http://localhost:8000/view/preview/rustacean-orig-noshadow.png
#+END_SRC

#+RESULTS:
#+BEGIN_SRC fundamental

GET http://localhost:8000/view/preview/rustacean-orig-noshadow.png
HTTP/1.1 200 OK
content-length: 3700
content-type: image/png
accept-ranges: bytes
last-modified: Fri, 13 Sep 2019 09:12:15 GMT
date: Fri, 13 Sep 2019 09:12:24 GMT
Request duration: 0.003336s
#+END_SRC

** =GET /view/full/:filename=
Query parameters:

| key      | type   | description                                             |
|----------+--------+---------------------------------------------------------|
| filename | ~String~ | The filename used in the =links= section                |

#+BEGIN_SRC restclient :session
GET http://localhost:8000/view/full/rustacean-orig-noshadow.png
#+END_SRC

#+RESULTS:
#+BEGIN_SRC fundamental

GET http://localhost:8000/view/full/rustacean-orig-noshadow.png
HTTP/1.1 200 OK
content-length: 48613
content-type: image/png
accept-ranges: bytes
last-modified: Fri, 13 Sep 2019 09:12:14 GMT
date: Fri, 13 Sep 2019 09:12:30 GMT
Request duration: 0.006981s
#+END_SRC
