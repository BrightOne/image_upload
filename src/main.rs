use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};
use std::io;
use std::path::PathBuf;
use warp::{http, Filter};

mod downloads;
mod images;

lazy_static! {
    static ref CONFIG: Config = envy::from_env().context(InvalidConfig).unwrap();
}

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("failed to download image ({})", source))]
    DownloadFailed { source: reqwest::Error },
    #[snafu(display("invalid request body ({})", source))]
    InvalidRequestBody { source: reqwest::Error },
    #[snafu(display("invalid config ({})", source))]
    InvalidConfig { source: envy::Error },
    #[snafu(display("no basename for file: {}", path.display()))]
    NoFileBasename { path: PathBuf },
    #[snafu(display("filesystem error ({})", source))]
    Filesystem { source: io::Error },
    #[snafu(display("image error ({})", source))]
    Image { source: image::ImageError },
}

#[derive(Debug, Deserialize, Serialize)]
struct UploadRequest {
    links: Vec<String>,
}

fn upload() -> impl Filter<Extract = (UploadRequest,), Error = warp::Rejection> + Copy {
    warp::post2()
        .and(warp::path("upload"))
        .and(warp::path::end())
        .and(warp::body::json::<UploadRequest>())
}

fn handle_upload(request: UploadRequest) -> Result<impl warp::Reply, warp::Rejection> {
    for link in request.links.iter() {
        let original =
            downloads::download_image(&link, &CONFIG.storage_path).map_err(warp::reject::custom)?;
        images::generate_preview(&original, &CONFIG.storage_path).map_err(warp::reject::custom)?;
    }
    Ok(http::StatusCode::OK)
}

#[derive(Clone, Debug, Deserialize)]
struct Config {
    server_port: u16,
    storage_path: PathBuf,
}

fn main() -> Result<(), Error> {
    pretty_env_logger::init();
    let port = CONFIG.server_port;
    let storage = CONFIG.storage_path.clone();

    let view = warp::path("view").and(warp::fs::dir(storage.clone()));
    let api = upload()
        .and_then(handle_upload)
        .or(view)
        .with(warp::log("image_upload"));

    warp::serve(api).run(([0, 0, 0, 0], port));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use warp::test::request;

    #[test]
    fn test_upload() {
        let filter = upload();
        let json = UploadRequest {
            links: vec![String::from(
                "https://docs.rs/rust-logo-20190213-1.34.0-nightly-e54494727.png",
            )],
        };
        let request = request().method("POST").path("/upload").json(&json);
        assert!(request.matches(&filter));
    }
}
