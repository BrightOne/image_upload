use super::*;
use image::{imageops::resize, FilterType};
use snafu::OptionExt;
use std::path::{Path, PathBuf};

pub(crate) fn generate_preview(image_path: &Path, dest: &Path) -> Result<PathBuf, Error> {
    let img = image::open(&image_path).context(Image)?;
    let basename = &image_path.file_name().context(NoFileBasename {
        path: image_path.to_path_buf(),
    })?;
    let resized = resize(&img, 100, 100, FilterType::Nearest);
    let preview_path = PathBuf::new().join(dest).join("preview").join(basename);
    resized.save(&preview_path).context(Filesystem)?;
    Ok(preview_path)
}
