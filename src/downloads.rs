use super::*;
use reqwest::get;
use snafu::{OptionExt, ResultExt};
use std::fs::File;
use std::path::{Path, PathBuf};

/// This function downloads an image from a URL and stores it
/// in `{path}/full`. Returns the path to saved image.
pub(crate) fn download_image<P>(url: &str, path: P) -> Result<PathBuf, Error>
where
    P: AsRef<Path>,
{
    let mut response = get(url).context(DownloadFailed)?;
    let url = PathBuf::from(url);
    let filename = url
        .file_name()
        .context(NoFileBasename { path: url.clone() })?;
    let path = PathBuf::new().join(path).join("full").join(filename);
    let mut file: File = File::create(&path).context(Filesystem)?;
    response.copy_to(&mut file).context(InvalidRequestBody)?;
    Ok(path)
}
